import fenstonsingel.os.coursework.matrices.SquareMatrix
import fenstonsingel.os.coursework.matrices.parallel.NaiveMatrixMultiplication
import org.junit.Assert
import org.junit.Test

class TimeComparisons {
    val bigDimension = 2000
    val bigFirst = SquareMatrix.random(bigDimension, 0, 10)
    val bigIdentity = SquareMatrix.identity(bigDimension)

    @Test
    fun sequentialMultiplication() {
        Assert.assertEquals(bigFirst, bigFirst * bigIdentity)
    }

    @Test
    fun parallelMultiplication() {
        Assert.assertEquals(bigFirst, NaiveMatrixMultiplication(bigFirst, bigIdentity))
    }
}