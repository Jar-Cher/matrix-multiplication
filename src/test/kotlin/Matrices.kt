import fenstonsingel.os.coursework.matrices.MutableSquareMatrix
import fenstonsingel.os.coursework.matrices.SquareMatrix
import org.junit.Test

class Matrices {
    val first = SquareMatrix.create(6, listOf(
        4, 8, 4, 0, 0, 0,
        8, 4, 0, 0, 0, 0,
        4, 0, 1, 0, 0, 0,
        0, 0, 0, 1, 0, 0,
        0, 0, 0, 0, 1, 0,
        0, 0, 0, 0, 0, 1
    ))
    val second = SquareMatrix.create(6, listOf(
        0, 1, 0, 0, 0, 0,
        1, 0, 0, 0, 0, 0,
        0, 0, 1, 0, 0, 0,
        0, 0, 0, 1, 0, 0,
        0, 0, 0, 0, 1, 0,
        0, 0, 0, 0, 0, 1
    ))

    @Test
    fun testPlus() {
        val new = first + second
        println(new)
    }

    @Test
    fun testTimes() {
        val new = first * second
        println(new)
    }

    val blocks = first.blocks(2)

    @Test
    fun testPlusOnBlocks() {
        val new = blocks[0] + blocks[3]
        println(first)
        println(new)
    }

    @Test
    fun testTimesOnBlocks() {
        val new = blocks[0] * blocks[3]
        println(first)
        println(new)
    }

    val newFirst = MutableSquareMatrix.zeroes(first.dimension).assign(first)
    val mutableBlocks = newFirst.mutableBlocks(2)

    @Test
    fun testPlusOnMutableBlocks() {
        mutableBlocks[1].assign(blocks[0] + blocks[3])
        println(newFirst)
    }

    @Test
    fun testTimesOnMutableBlocks() {
        mutableBlocks[2].assign(blocks[0] * blocks[3])
        println(newFirst)
    }
}