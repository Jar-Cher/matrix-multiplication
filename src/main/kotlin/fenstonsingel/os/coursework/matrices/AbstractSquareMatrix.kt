package fenstonsingel.os.coursework.matrices

abstract class AbstractSquareMatrix : MutableSquareMatrix {
    override fun assign(other: SquareMatrix): MutableSquareMatrix {
        require(dimension == other.dimension)
        for (row in 0 until dimension) {
            for (column in 0 until dimension) {
                this[row, column] = other[row, column]
            }
        }
        return this
    }

    protected fun plus(other: SquareMatrix, dest: MutableSquareMatrix): SquareMatrix {
        require(dimension == other.dimension)
        for (row in 0 until dimension) {
            for (column in 0 until dimension) {
                dest[row, column] = this[row, column] + other[row, column]
            }
        }
        return dest
    }

    protected fun times(other: SquareMatrix, dest: MutableSquareMatrix): SquareMatrix {
        require(dimension == other.dimension)
        for (row in 0 until dimension) {
            for (column in 0 until dimension) {
                for (i in 0 until dimension) {
                    dest[row, column] += this[row, i] * other[i, column]
                }
            }
        }
        return dest
    }

    override fun toString(): String {
        val sb = StringBuilder()
        for (row in 0 until dimension) {
            for (column in 0 until dimension) {
                sb.append("${this[row, column]} ")
            }
            sb.append("\n")
        }
        return sb.toString()
    }
}