package fenstonsingel.os.coursework.matrices

class SquareMatrixBlock(
    override val dimension: Int,
    private val delegate: MutableSquareMatrix,
    private val blockRow: Int,
    private val blockColumn: Int
) : AbstractSquareMatrix() {
    override fun get(row: Int, column: Int): Int {
        require(row in 0 until dimension)
        require(column in 0 until dimension)
        return delegate[blockRow * dimension + row, blockColumn * dimension + column]
    }

    override fun set(row: Int, column: Int, value: Int) {
        require(row in 0 until dimension)
        require(column in 0 until dimension)
        delegate[blockRow * dimension + row, blockColumn * dimension + column] = value
    }

    override fun blocks(dimension: Int): List<SquareMatrix> {
        TODO("Not yet implemented")
    }

    override fun mutableBlocks(dimension: Int): List<MutableSquareMatrix> {
        TODO("Not yet implemented")
    }

    override fun plus(other: SquareMatrix): SquareMatrix {
        val newMatrix = MutableSquareMatrix.zeroes(dimension)
        return plus(other, newMatrix)
    }

    override fun times(other: SquareMatrix): SquareMatrix {
        val newMatrix = MutableSquareMatrix.zeroes(dimension)
        return times(other, newMatrix)
    }
}