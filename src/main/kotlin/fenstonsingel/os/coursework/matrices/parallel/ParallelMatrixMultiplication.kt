package fenstonsingel.os.coursework.matrices.parallel

import fenstonsingel.os.coursework.matrices.SquareMatrix

interface ParallelMatrixMultiplication {
    operator fun invoke(first: SquareMatrix, second: SquareMatrix, divisionDimension: Int)
}