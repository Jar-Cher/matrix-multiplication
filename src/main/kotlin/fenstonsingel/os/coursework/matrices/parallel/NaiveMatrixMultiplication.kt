package fenstonsingel.os.coursework.matrices.parallel

import fenstonsingel.os.coursework.matrices.MutableSquareMatrix
import fenstonsingel.os.coursework.matrices.SquareMatrix
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

object NaiveMatrixMultiplication : ParallelMatrixMultiplier() {
    override fun multiply(
        firstBlocks: List<SquareMatrix>,
        secondBlocks: List<SquareMatrix>,
        resultBlocks: List<MutableSquareMatrix>,
        divisionDimension: Int
    ) {
        val numberOfThreads = resultBlocks.size

        fun calculateBlock(blockRow: Int, blockColumn: Int) {
            val resultBlock = resultBlocks[blockRow * divisionDimension + blockColumn]
            for (i in 0 until divisionDimension) {
                val fi = blockRow * divisionDimension + i
                val firstBlock = firstBlocks[blockRow * divisionDimension + i]
                val si = i * divisionDimension + blockColumn
                val secondBlock = secondBlocks[i * divisionDimension + blockColumn]
                resultBlock.assign(resultBlock + (firstBlock * secondBlock))
            }
        }

        val executorService = Executors.newFixedThreadPool(numberOfThreads)
        for (row in 0 until divisionDimension) {
            for (column in 0 until divisionDimension) {
                executorService.submit { calculateBlock(row, column) }
            }
        }
        executorService.shutdown()
        while (!executorService.isTerminated) {
            executorService.awaitTermination(1L, TimeUnit.MILLISECONDS)
        }
    }
}