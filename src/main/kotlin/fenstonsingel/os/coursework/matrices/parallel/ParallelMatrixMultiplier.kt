package fenstonsingel.os.coursework.matrices.parallel

import fenstonsingel.os.coursework.matrices.MutableSquareMatrix
import fenstonsingel.os.coursework.matrices.SquareMatrix

abstract class ParallelMatrixMultiplier {
    operator fun invoke(first: SquareMatrix, second: SquareMatrix): SquareMatrix {
        require(first.dimension == second.dimension)
        val maxNumberOfThreads = Runtime.getRuntime().availableProcessors()
        val divisionDimension = calculateDivisionDimension(first.dimension, maxNumberOfThreads)
        require(divisionDimension != null)

        val result = MutableSquareMatrix.zeroes(first.dimension)
        val firstBlocks = first.blocks(divisionDimension)
        val secondBlocks = second.blocks(divisionDimension)
        val resultBlocks = result.mutableBlocks(divisionDimension)

        multiply(firstBlocks, secondBlocks, resultBlocks, divisionDimension)

        return result
    }

    private fun calculateDivisionDimension(dimension: Int, maxNumberOfThreads: Int): Int? {
        var result: Int? = null
        for (currDivisionDimension in generateSequence(2) { it + 1 }) {
            if (currDivisionDimension * currDivisionDimension > maxNumberOfThreads) break
            if (dimension % currDivisionDimension == 0) result = currDivisionDimension
        }
        return result
    }

    abstract fun multiply(
        firstBlocks: List<SquareMatrix>,
        secondBlocks: List<SquareMatrix>,
        resultBlocks: List<MutableSquareMatrix>,
        divisionDimension: Int
    )
}