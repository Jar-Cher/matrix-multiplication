package fenstonsingel.os.coursework.matrices

class SquareMatrixImpl(
    override val dimension: Int,
    private val contents: MutableList<Int>
) : AbstractSquareMatrix() {
    override fun get(row: Int, column: Int): Int {
        require(row in 0 until dimension)
        require(column in 0 until dimension)
        return contents[row * dimension + column]
    }

    override fun set(row: Int, column: Int, value: Int) {
        require(row in 0 until dimension)
        require(column in 0 until dimension)
        synchronized(contents) {
            contents[row * dimension + column] = value
        }
    }

    override fun blocks(divisionDimension: Int): List<SquareMatrix> =
        mutableBlocks(divisionDimension)

    override fun mutableBlocks(divisionDimension: Int): List<MutableSquareMatrix> {
        require(dimension % divisionDimension == 0)
        val list = mutableListOf<SquareMatrixBlock>()
        for (row in 0 until divisionDimension) {
            for (column in 0 until divisionDimension) {
                list.add(SquareMatrixBlock(dimension / divisionDimension, this, row, column))
            }
        }
        return list
    }

    override fun plus(other: SquareMatrix): SquareMatrix {
        val newMatrix = MutableSquareMatrix.zeroes(dimension)
        return plus(other, newMatrix)
    }

    override fun times(other: SquareMatrix): SquareMatrix {
        val newMatrix = MutableSquareMatrix.zeroes(dimension)
        return times(other, newMatrix)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SquareMatrixImpl

        if (contents != other.contents) return false

        return true
    }

    override fun hashCode(): Int {
        return contents.hashCode()
    }
}