package fenstonsingel.os.coursework.matrices

interface MutableSquareMatrix : SquareMatrix {
    operator fun set(row: Int, column: Int, value: Int)

    fun assign(other: SquareMatrix): MutableSquareMatrix

    fun mutableBlocks(divisionDimension: Int): List<MutableSquareMatrix>

    companion object {
        fun zeroes(dimension: Int): MutableSquareMatrix {
            return SquareMatrixImpl(
                dimension, MutableList(dimension * dimension) { 0 }
            )
        }
    }
}