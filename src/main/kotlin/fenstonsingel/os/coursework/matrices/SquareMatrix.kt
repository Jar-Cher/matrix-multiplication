package fenstonsingel.os.coursework.matrices

import kotlin.random.Random

interface SquareMatrix {
    val dimension: Int

    operator fun get(row: Int, column: Int): Int

    fun blocks(divisionDimension: Int): List<SquareMatrix>

    operator fun plus(other: SquareMatrix): SquareMatrix

    operator fun times(other: SquareMatrix): SquareMatrix

    companion object {
        fun create(dimension: Int, contents: List<Int>): SquareMatrix {
            require(dimension * dimension == contents.size)
            return SquareMatrixImpl(dimension, contents.toMutableList())
        }

        fun random(dimension: Int, from: Int, to: Int): SquareMatrix {
            return SquareMatrixImpl(
                dimension, MutableList(dimension * dimension) {
                    Random.nextInt(from, to)
                }
            )
        }

        fun identity(dimension: Int): SquareMatrix {
            return SquareMatrixImpl(
                dimension, MutableList(dimension * dimension) {
                    if (it % (dimension + 1) == 0) 1 else 0
                }
            )
        }
    }
}